\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{xspace}

\usepackage{amsmath,amssymb}
\usepackage{stmaryrd}
\usepackage{centernot}

\newcommand\sA{\mathcal{A}}
\newcommand\sAnk{\mathcal{A}\sp*}
\newcommand\sK{\mathcal{K}}
\newcommand\intE[2]{{\llbracket #1,#2 \rrbracket}}
\newcommand\deter{\longrightarrow}
\newcommand\ndeter{\centernot\longrightarrow}
\newcommand\p[1]{{\left(#1\right)}}

\newenvironment{interp}{%
  \textit{Interprétation}:
}{%
}

\title{Dépendances fonctionnelles et normalisation: formalisation}

\begin{document}

\maketitle

\section{Notations}

Soit $R$ une relation, on notera :

\begin{itemize}
  \item $\sA$ l'ensemble des attributs,
  \item $\sK$ l'ensemble des clefs, (attention $\sK$ est composée de clefs qui
    sont des
    sous-ensembles de $\sA$: $\forall K\in\sK,\; K\subset \sA$),
  \item $\sAnk = \sA \backslash \p{\bigcup_{K\in\sK} K}$ l'ensemble des
    attributs qui n'interviennent dans aucune clef,
  \item pour $a\in\sA$ et $i\in\intE1n$, on notera $a_i$ la valeur prise par
    l'attribut $a$ dans l'enregistrement $i$ de la relation $R$.
  \item pour deux ensemble $E$ et $F$,
    \begin{itemize}
      \item on notera $E\subset F$ si $E$ est inclus dans $F$,
      \item on notera $E\subsetneq F$ si $E$ est strictement inclus dans $F$.
    \end{itemize}
\end{itemize}

Tout le raisonnement s'effectuera dans le cadre de la relation $R$ avec les
notations précités.

\section{Définitions}

\subsection{Dépendance fonctionnelle}

Pour tout couple d'attribut $(x,y)\in\sA^2$, on définira la dépendance
fonctionnelle $x\deter y$ si et seulement si:

\[
  \forall (i,j)\in\intE1n^2,\quad x_i=x_j\Longrightarrow y_i=y_j
\]

\begin{interp}
  Si sur deux enregistrements, l'attribut $x$ a la même valeur, alors $y$
  également.
\end{interp}

\subsection{Clef}

Soit $G\subset \sA$, on dira que $G$ est une clef (c'est à dire que
$G\in\sK$) si et seulement si:

\begin{itemize}
  \item $G$ est non-vide:
    \[
      \exists a\in\sA,\quad a\in G
    \]
    \begin{interp}
      Il existe au moins un attribut dans $G$.
    \end{interp}
  \item $G$ détermine tout les attributs:
    \[
      \forall a\in\sA,\quad G\deter a
    \]
    \begin{interp}
      Tout attribut est déterminé par $G$.
    \end{interp}

    \vspace{1em}
  \item $G$ est minimale:
    \[
      \nexists G'\subsetneq G,\quad \forall a\in\sA,\quad G'\deter a
    \]
    \begin{interp}
      Il n'existe pas de sous-ensemble strict de $G$ qui détermine tout les
      attributs.
    \end{interp}
    
    \vspace{1em}
    Formulation équivalente:
    \[
      \forall G'\subsetneq G,\quad \exists a\in\sA,\quad G'\ndeter a
    \]
    \begin{interp}
      Pour toute sous-ensemble strict de $G$, il existe un attribut qui n'est pas
      déterminé par ce sous-ensemble.
    \end{interp}
\end{itemize}

\section{Formes normales}

\subsection{1NF}

Une relation respecte la 1NF si et seulement si:
\begin{itemize}
  \item Tous les attributs sont atomiques.

    \vspace{1em}
  \item Il existe une clef:
    \[
      \sK \neq \varnothing
    \]
    \begin{interp}
      L'ensemble des clefs est non vide.
    \end{interp}
\end{itemize}

\subsection{2NF}

Une relation respecte la 2NF si et seulement si:
\begin{itemize}
  \item La relation respecte la 1NF.
    \vspace{1em}
  \item Aucun sous-ensemble strict d'une clef ne determine un attribut non membre d'une clef:
    \[
      \forall a\in\sAnk,\quad\forall K\in\sK,\quad\nexists G\subsetneq K,\quad G\deter a
    \]
    \begin{interp}
      Pour tout attribut non membre d'une clef ($a$) , on ne peut pas construire
      un sous-ensemble strict d'une clef qui détermine cet attribut.
    \end{interp}

    \vspace{1em}
    Formulation équivalente:
    \[
      \forall K\in\sK,\quad\forall G\subsetneq K,\quad\forall a\in\sAnk,\quad G\ndeter a
    \]
    \begin{interp}
      Pour toute sous-ensemble $(G$) strict d'une clef et pour tout attribut ($a$) non
      membre d'une clef, $G$ ne détermine pas $a$.
    \end{interp}
\end{itemize}

\subsection{3NF}

Une relation respecte la 3NF si et seulement si:
\begin{itemize}
  \item La relation respecte la 1NF.
    \vspace{1em}
  \item La seule manière de déterminer un attribut non membre d'une clef est
    d'utiliser une clef.

    \[
      \forall a\in\sAnk,\quad\forall G\subset\sA\backslash\{a\},\quad
      G\deter a\Longrightarrow \exists K\in\sK,\;K\subset G
    \]
    \begin{interp}
      Si on arrive à déterminer un attribut non membre d'une clef ($a$) par un
      groupe d'attribut (ne contenant pas $a$), alors ce groupe d'attribut
      contient une clef.
    \end{interp}
\end{itemize}

\subsection{BCNF}

Une relation respecte la BCNF si et seulement si:
\begin{itemize}
  \item La relation respecte la 1NF.
    \vspace{1em}
  \item La seule manière de déterminer un attribut est
    d'utiliser une clef.

    \[
      \forall a\in\sA,\quad\forall G\subset\sA\backslash\{a\},\quad
      G\deter a\Longrightarrow \exists K\in\sK,\;K\subset G
    \]
    \begin{interp}
      Si on arrive à déterminer un attribut ($a$) par un
      groupe d'attribut (ne contenant pas $a$), alors ce groupe d'attribut
      contient une clef.
    \end{interp}
\end{itemize}
      
\end{document}
    
    \vspace{1em}
    Formulation équivalente:
    \[
      \forall a\in\sAnk,\quad \forall G\subset\sA\backslash\{a\}
